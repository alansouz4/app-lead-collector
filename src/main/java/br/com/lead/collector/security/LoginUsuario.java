package br.com.lead.collector.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class LoginUsuario implements UserDetails {

    private long id;
    private String email;
    private String senha;

    public LoginUsuario(long id, String email, String senha) {
        this.id = id;
        this.email = email;
        this.senha = senha;
    }

    public LoginUsuario() {
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() { // níveis de autorização
        return null;
    }

    @Override
    public String getPassword() { // padrao utilizado no login do usuario
        return senha;
    }

    @Override
    public String getUsername() { // padrao utilizado no login do usuario
        return email;
    }

    @Override
    public boolean isAccountNonExpired() { // MÉTODO PARA EXPIRAR A CONTA
        return true;
    }

    @Override
    public boolean isAccountNonLocked() { // CONTA DESBLOQUEADA ?
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() { // CONTA INSPIRADA ?
        return true;
    }

    @Override
    public boolean isEnabled() { // CONTA ATIVA ?
        return true;
    }
}
