package br.com.lead.collector.security;

import br.com.lead.collector.models.dtos.CredenciaisDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class FiltroDeAutenticacao extends UsernamePasswordAuthenticationFilter {

    private AuthenticationManager authenticationManager;
    private JWTUtil jwtUtil;

    public FiltroDeAutenticacao(AuthenticationManager authenticationManager, JWTUtil jwtUtil) {
        this.authenticationManager = authenticationManager;
        this.jwtUtil = jwtUtil;
    }

    // MÉTODO PARA AUTENTICAÇÃO DA REQUISICAO
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {

        // CONVERTENDO REQUEST PARA DTO
        ObjectMapper objectMapper = new ObjectMapper();

        try{
            CredenciaisDTO credenciais = objectMapper.readValue(request.getInputStream(), CredenciaisDTO.class);

            // VERIFICA USUARIO E VALIDA SE ELE PODE OU NÃO POSSUIR UM TOKEN
            UsernamePasswordAuthenticationToken usernameToken = new UsernamePasswordAuthenticationToken(
                    credenciais.getEmail(), credenciais.getSenha(), new ArrayList<>());

            // FAZENDO AUTENTICAÇÃO DO TOKEN DO USUARIO NO SISTEMA
            Authentication authentication = authenticationManager.authenticate(usernameToken);
            return authentication;

        }catch (IOException e){
            throw new RuntimeException(e.getMessage());
        }
    }

    // RESPONDENDO AO USUÁRIO A AUTENTICAÇÃO BEM SUCEDIDA E GERA O TOKEN
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
                                            Authentication authResult) throws IOException, ServletException {
        // ALTERA O USUARIO DTO QUE CONVERTEMOS EM UM LOGIN DE USUÁRIO E PEGA O USER NAME DELE
        String username = ((LoginUsuario) authResult.getPrincipal()).getUsername();

        // PASSANDO O MÉTODO QUE EGERA O TOKEN
        String token = jwtUtil.gerarToken(username);

        // REPONDENDO O TOKEN PELO HEADER
        response.addHeader("Authorization", "Bearer " + token);
    }
}
