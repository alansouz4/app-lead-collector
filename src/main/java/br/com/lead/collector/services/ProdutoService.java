package br.com.lead.collector.services;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    public Produto salvarProduto(Produto produto){
        Produto produtoObjeto = produtoRepository.save(produto);
        return produtoObjeto;
    }

    public Iterable<Produto> buscarTodosProduto(){
        Iterable<Produto> produto = produtoRepository.findAll();
        return produto;
    }

    public Produto buscarPorId(Long id){
        Optional<Produto> optionalProduto = produtoRepository.findById(id);
        if(optionalProduto.isPresent()){
            return optionalProduto.get();
        }
        throw new RuntimeException("Produto não encontrado!");
    }

    public Iterable<Produto> buscarTodosProdutosPorNome(String nome){
        Iterable<Produto> produto = produtoRepository.findAllByNome(nome);
        return produto;
    }

    public Produto atualizarProduto(Long id, Produto produto){
        if(produtoRepository.existsById(id)){
            produto.setId(id);
            Produto produtoObjeto = salvarProduto(produto);
            return produtoObjeto;
        }
        throw new RuntimeException("O produto não foi localizado!");
    }

    public void deletarProduto(Long id){
        if(produtoRepository.existsById(id)){
            produtoRepository.deleteById(id);
        } else{
            throw new RuntimeException("Produto não encontrado");
        }
    }
    public List<Produto> buscarTodosPeloId(List<Long> id){
        Iterable<Produto> produto = produtoRepository.findAllById(id);
        return (List) produto;
    }
}
