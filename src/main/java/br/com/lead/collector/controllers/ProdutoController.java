package br.com.lead.collector.controllers;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.management.RuntimeErrorException;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Produto registrarProduto(@RequestBody Produto produto){
        return produtoService.salvarProduto(produto);
    }
    @GetMapping
    public Iterable<Produto> buscarTodosProduto(@RequestParam(name = "nome", required = false) String nome){
        if(nome != null){
            Iterable<Produto> produto = produtoService.buscarTodosProdutosPorNome(nome);
            return  produto;
        }
        Iterable<Produto> produto = produtoService.buscarTodosProduto();
        return  produto;
    }
    @GetMapping("/{id}")
    public Produto buscarPorId(@PathVariable(name = "id") Long id){
        try{
            Produto produto = produtoService.buscarPorId(id);
            return produto;
        }catch (RuntimeErrorException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
    @PutMapping("/{id}")
    public Produto atualizarProduto(@PathVariable(name = "id") Long id, @RequestBody Produto produto){
        try{
            Produto produtoObjeto = produtoService.atualizarProduto(id, produto);
            return produtoObjeto;
        }catch (RuntimeErrorException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletarProduto(@PathVariable(name = "id") Long id){
        try{
            produtoService.deletarProduto(id);
            return ResponseEntity.status(204).body("");
        }catch (RuntimeErrorException exccption){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exccption.getMessage());
        }
    }
}
