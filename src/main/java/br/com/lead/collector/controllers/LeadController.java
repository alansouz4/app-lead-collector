package br.com.lead.collector.controllers;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.services.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/leads")
public class LeadController {

    @Autowired
    private LeadService leadService;

    @PostMapping
    // resposta 201
    @ResponseStatus(HttpStatus.CREATED)
    public Lead registrarLead(@RequestBody @Valid Lead lead){
        return leadService.salvarLead(lead);
    }

    @GetMapping
    @CrossOrigin(origins = "http://*.itau.com.br") // libera o acesso deste endpoint via cors
    public Iterable<Lead> exibirTodos(@RequestParam(name = "tipoDeLead", required = false)TipoLeadEnum tipoLeadEnum){
        if(tipoLeadEnum != null){
            Iterable<Lead> leads = leadService.buscarTodosPorTipoLead(tipoLeadEnum);
            return leads;
        }
        Iterable<Lead> leads = leadService.buscarTodos();
        return leads;
    }

    @GetMapping("/{id}")
    public Lead buscarPorId(@PathVariable(name = "id") Long id){
        try{
            Lead lead = leadService.buscarPorId(id);
            return lead;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Lead> atualizarLead(@PathVariable(name = "id") Long id, @RequestBody Lead lead){
        try{
            Lead leadObjeto = leadService.atualizarLead(id, lead);
            return ResponseEntity.ok(leadObjeto); // usando ResponseEntity para manipular o status de retorno
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    // @ResponseStatus(HttpStatus.NO_CONTENT) - posso retornar o status 204 por aqui
    public ResponseEntity<?> deletarLead(@PathVariable Long id){
        try{
            leadService.deletarLead(id);
            return ResponseEntity.status(204).body(""); // usando ResponseEntity para manipular o status de retorno
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
