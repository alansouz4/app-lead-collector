package br.com.lead.collector.exceptions.errors;

import java.util.HashMap;

public class MensagemDeErro {
    private String erro;
    private String mensagemDeErro;
    private HashMap<String, ObjetoErro> camposDeErro;

    public MensagemDeErro(String erro, String mensagemDeErro, HashMap<String, ObjetoErro> camposDeErro) {
        this.erro = erro;
        this.mensagemDeErro = mensagemDeErro;
        this.camposDeErro = camposDeErro;
    }

    public MensagemDeErro(){
    }

    public String getError() { return erro; }

    public void setError(String error) {
        this.erro = error;
    }

    public String getMensagemDeErro() {
        return mensagemDeErro;
    }

    public void setMensagemDeErro(String mensagemDeErro) {
        this.mensagemDeErro = mensagemDeErro;
    }

    public HashMap<String, ObjetoErro> getCamposDeErro() {
        return camposDeErro;
    }

    public void setCamposDeErro(HashMap<String, ObjetoErro> camposDeErro) {
        this.camposDeErro = camposDeErro;
    }
}
