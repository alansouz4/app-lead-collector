package br.com.lead.collector.exceptions;

import br.com.lead.collector.exceptions.errors.MensagemDeErro;
import br.com.lead.collector.exceptions.errors.ObjetoErro;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.List;

@ControllerAdvice
public class ErrorHendle {

    // resposta de erro do properties
    @Value("${mensagem.padrao.de.validacao}")
    private String mensagemPadrao;

    @ExceptionHandler(MethodArgumentNotValidException.class)
    // resposta padrão do processamento
    @ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY)
    // retorna da resposta no corpo
    @ResponseBody
    // dentro do metodo MethodArgumentNotValidException tem o objeto Binding
    public MensagemDeErro manipulacaoDeErrosDeValidacao(MethodArgumentNotValidException exception){
        HashMap<String, ObjetoErro> erros = new HashMap<>();

        // pegando o objeto getBindingResult do método MethodArgumentNotValidException
        BindingResult resultado = exception.getBindingResult();

        // lista de erros armazenados no Binding
        List<FieldError> fieldErrors = resultado.getFieldErrors();

        // percorrendo a lista
        for(FieldError erro : fieldErrors){
            // desvolve o campo e a mensagem de erro
            erros.put(erro.getField(), new ObjetoErro(erro.getDefaultMessage(),
                    erro.getRejectedValue().toString()));
        }

        MensagemDeErro mensagemDeErro = new MensagemDeErro(HttpStatus.UNPROCESSABLE_ENTITY.toString(),
                mensagemPadrao, erros);
        return mensagemDeErro;
    }
}
