package br.com.lead.collector.models;

import br.com.lead.collector.enums.TipoLeadEnum;
//import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

@Entity
// ignora o campo data vindo pelo json e vai preencher como null
//@JsonIgnoreProperties(value = {"data"}, allowGetters = true)
public class Lead {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(min = 4, max = 100, message = "O nome deve ter entre 4 e 100 caracteres")
    @NotNull(message = "Nome não pode ser nulo")
    private String nome;

    @Email(message = "O formato do email é inválido")
    @NotNull(message = "Email não pode ser nulo")
    private String email;

    private LocalDate data;

    @NotNull(message = "Nome não pode ser nulo")
    //@Enumerated(EnumType.STRING)
    private TipoLeadEnum tipoLead;



    // cascade = CascadeType.ALL - diz se um lead for apagado, apaga a tabela de relacionamento que contem os produtos
    @ManyToMany(cascade = CascadeType.ALL)
    private List<Produto> produtos;

    // mapeamento das tabelas do banco dizendo os relacionamentos
//    @JoinTable(name = "nomeTabela",
//            joinColumns = @JoinColumn(name = "lead_id"),
//            inverseJoinColumns = @JoinColumn(name = "product_id"))



    public Lead() {
    }


    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public TipoLeadEnum getTipoLead() {
        return tipoLead;
    }

    public void setTipoLead(TipoLeadEnum tipoLead) {
        this.tipoLead = tipoLead;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }
}
