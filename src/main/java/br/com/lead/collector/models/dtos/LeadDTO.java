package br.com.lead.collector.models.dtos;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

public class LeadDTO {

    @Size(min = 5, max = 100, message = "O nome deve ter entre 5 à 10 caracteres")
    @NotNull(message = "Nome não pode ser nullo")
    @NotBlank(message = "Não pode ser só espaço")
    private String nome;

    @Email(message = "O formato do email é invalido")
    @NotNull(message = "Email não pode ser nullo")
    private String email;

    @NotNull(message = "Tipo do Lead não pode ser nullo")
    private TipoLeadEnum tipoLead;

    public LeadDTO(){
    }

    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public TipoLeadEnum getTipoLead() {
        return tipoLead;
    }

    public void setTipoLead(TipoLeadEnum tipoLead) {
        this.tipoLead = tipoLead;
    }

    public Lead converterParaLead(){
        LocalDate data = LocalDate.now();
        Lead lead = new Lead();
        lead.setNome(this.nome);
        lead.setEmail(this.email);
        lead.setTipoLead(this.tipoLead);
        lead.setData(data);
        return lead;
    }
}
