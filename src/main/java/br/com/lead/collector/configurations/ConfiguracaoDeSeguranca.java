package br.com.lead.collector.configurations;

import br.com.lead.collector.security.FiltroDeAutenticacao;
import br.com.lead.collector.security.JWTUtil;
import br.com.lead.collector.services.FiltroAutorizacao;
import br.com.lead.collector.services.LoginUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@EnableWebSecurity
public class ConfiguracaoDeSeguranca extends WebSecurityConfigurerAdapter {

    @Autowired
    private JWTUtil jwtUtil;

    @Autowired
    private LoginUsuarioService loginUsuarioService;

    private static final String[] PUBLIC_MATCHERS_GET = {
            "/leads",
            "/leads/**",
            "/produtos"
    };

    private static final String[] PUBLIC_MATCHERS_POST = {
            "/leads",
            "/leads/**",
            "/produtos",
            "/produtos/**",
            "/usuario",
            "/login"
    };

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // DESABILITANDO PREENCHIMENTO DE TOKEN DE FORMULARIO
        http.csrf().disable();
        // LIBERANDO O CORS
        http.cors();

        // DANDO ACESSO TOTAL PARA AS REQUISIÇÕES
        http.authorizeRequests()
                .antMatchers(HttpMethod.GET, PUBLIC_MATCHERS_GET).permitAll()
                .antMatchers(HttpMethod.POST, PUBLIC_MATCHERS_POST).permitAll()
                .anyRequest().authenticated();

        // ELIMINANDO SESSÃO
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        // FILTROS JWT
        http.addFilter(new FiltroDeAutenticacao(authenticationManager(), jwtUtil));
        http.addFilter(new FiltroAutorizacao(authenticationManager(), jwtUtil, loginUsuarioService));
    }

    // CONFIGURAÇÃO PARA LIBERAÇÃO DO CORS
    @Bean
    CorsConfigurationSource configurationDeCors(){
        final UrlBasedCorsConfigurationSource cors = new UrlBasedCorsConfigurationSource();
        cors.registerCorsConfiguration("/**", new CorsConfiguration()
            .applyPermitDefaultValues()); // libera todas permissões para urls do sistemas
        return cors;
    }

    // Cria objeto BCrypt para servi-lo pra quem chamar
    @Bean
    BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

    // SOBSCREVER
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(loginUsuarioService).passwordEncoder(bCryptPasswordEncoder());
    }
}
