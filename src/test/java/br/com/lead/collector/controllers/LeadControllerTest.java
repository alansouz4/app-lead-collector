package br.com.lead.collector.controllers;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.LeadService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebMvcTest(LeadController.class)
public class LeadControllerTest {

    @MockBean
    private LeadService leadService;

    @Autowired
    private MockMvc mockMvc;

    Lead lead;
    Produto produto;
    List<Produto> produtos;
    List<Lead> leads;

    @BeforeEach
    public void setUp(){
        lead = new Lead();
        lead.setId(1L);
        lead.setNome("Alan");
//        lead.setData(LocalDate.now());
        lead.setTipoLead(TipoLeadEnum.QUENTE);
        lead.setEmail("souz4alan@gmail.com");

        leads = new ArrayList<>();
        leads.add(lead);

        produto = new Produto();
        produto.setDescricao("Café do boum");
        produto.setId(1L);
        produto.setNome("Café");
        produto.setPreco(30.00);

        produtos = new ArrayList<>();
        produtos.add(produto);

        lead.setProdutos(produtos);
    }

    @Test
    public void testarRegistrarLeadValido() throws Exception {
        Mockito.when(leadService.salvarLead((Mockito.any(Lead.class)))).then(leadObjeto -> {
            lead.setId(1L);
            lead.setData(LocalDate.now());
            return lead;
        });

        // converte o objeto lead em json pelos gets e sets
        ObjectMapper mapper = new ObjectMapper();
        String jsonDoLead = mapper.writeValueAsString(lead);

        // FAZENDO REQUISIÇÃO
        // perform - método responsavel por fazer as requisições
        // MockMvcRequestBuilders - monta as requisições
        // contentType(MediaType.APPLICATION_JSON) - tipo da nossa comunicação que informamos na headers
        // content(jsonDoLead)) - O json que convertemos
        // andExpect(MockMvcResultMatchers.status().isCreated() - verifica se esta me retornando 201 created
        // andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)) - batendo pelo campo
        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDoLead))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data", CoreMatchers.equalTo(LocalDate.now().toString())));
    }

    @Test
    public void testarExibirTodosLeds() throws Exception {
        Mockito.when(leadService.buscarTodos()).thenReturn(leads);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1));
    }

    @Test
    public void testarBuscarPorId() throws Exception {
        lead.setId(1L);
        Mockito.when(leadService.buscarPorId(Mockito.anyLong())).thenReturn(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/1")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarDeletarLead() throws Exception {
        //Mockito.when(this.leadService.deletarLead(1L)).thenReturn(1L);

        mockMvc.perform(MockMvcRequestBuilders.delete("/leads/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));

    }

}
