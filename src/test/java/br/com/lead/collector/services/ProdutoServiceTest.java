package br.com.lead.collector.services;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Arrays;

@SpringBootTest
public class ProdutoServiceTest {

    @MockBean
    private ProdutoRepository produtoRepository;

    @Autowired
    private ProdutoService produtoService;

    Produto produto;

    @BeforeEach
    public void setUp(){
        produto = new Produto();
        produto.setId(1L);
        produto.setNome("joão");
        produto.setDescricao("Gente fina");
        produto.setPreco(1.0);
    }

    @Test
    public void testarRetonarSucesso_QuandoCriarProduto(){
        Mockito.when(produtoRepository.save(produto)).thenReturn(produto);

        Produto produtoObjeto = produtoService.salvarProduto(produto);

        Assertions.assertSame(produto, produtoObjeto);
    }

    @Test
    public void testarNaoRetornar_QuandoCriarProdutos(){
        Mockito.when(produtoRepository.save(null)).thenReturn(produto);

        Produto produtoObjeto = produtoService.salvarProduto(null);

        Assertions.assertSame(produto, produtoObjeto);
    }

    @Test
    public void testarNaoRetornar_QuandoBuscarTodosProdutos(){

        Iterable<Produto> produtos = Arrays.asList(produto);

        Mockito.when(produtoRepository.findAll()).thenReturn(produtos);

        Iterable<Produto> produtosIterable = produtoService.buscarTodosProduto();

        Assertions.assertEquals(produto, produtosIterable);
    }

}
