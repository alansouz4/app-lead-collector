package br.com.lead.collector.services;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class LeadServiceTest {

    @MockBean
    private LeadRepository leadRepository;
    @MockBean
    private ProdutoService produtoService;

    @Autowired
    private LeadService leadService;

    Lead lead;
    Produto produto;
    List<Produto> produtos;

    @BeforeEach
    public void setUp(){
        lead = new Lead();
        lead.setId(1L);
        lead.setNome("Alan");
//        lead.setData(LocalDate.now());
        lead.setTipoLead(TipoLeadEnum.QUENTE);
        lead.setEmail("souz4alan@gmail.com");

        produto = new Produto();
        produto.setDescricao("Café do boum");
        produto.setId(1L);
        produto.setNome("Café");
        produto.setPreco(30.00);
        produtos = new ArrayList<>();
        produtos.add(produto);
        lead.setProdutos(produtos);
    }

    @Test
    public void testarPorTodosOsLeads(){

        // chamamos o lead criado no setUp
        Iterable<Lead> leads = Arrays.asList(lead);
        // capturando a chamado do método e passando o lead criado no setUp passado na lista
        Mockito.when(leadRepository.findAll()).thenReturn(leads);

        // buscando dados pelo repository, pelo método no service mokado (ou seja, em uma réplica)
        Iterable<Lead> leadIterable = leadService.buscarTodos();

        // compara lead criado, com lead buscado no repository
        Assertions.assertEquals(leads, leadIterable);
    }

    @Test
    public void testarSalvarLead(){
        // anyList - qql lista que vir do MÉTODO, eu captura a chamada e entrego os produtos que estão la em cima.
        Mockito.when(produtoService.buscarTodosPeloId(Mockito.anyList())).thenReturn(produtos);

        Lead leadTeste = new Lead();
        leadTeste.setNome("Jeff");
        leadTeste.setProdutos(produtos);
        leadTeste.setEmail("lan@yah");
        leadTeste.setTipoLead(TipoLeadEnum.FRIO);

        // anyList - qql classe que vir do MÉTODO, eu captura a chamada e entrego o lead que esta la em cima.
        Mockito.when(leadRepository.save(leadTeste)).thenReturn(leadTeste);

        Lead leadObejeto = leadService.salvarLead(leadTeste);

        // compara as datas
        Assertions.assertEquals(LocalDate.now(), leadObejeto.getData());
        // compara o produto com o produto do repository
        Assertions.assertEquals(produto, leadObejeto.getProdutos().get(0));
    }

    @Test
    public void testarDeletarLead(){
        Mockito.when(leadRepository.existsById(Mockito.anyLong())).thenReturn(true);

        leadService.deletarLead(1L);

        Mockito.verify(leadRepository, Mockito.times(1)).deleteById(1L);

    }
}
